/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

/**
 *
 * @author Janderson
 */
public class InfoServer {
    public String id;
    public String ip;
    public String porta;

    public InfoServer(String id, String ip, String porta) {
        this.id = id;
        this.ip = ip;
        this.porta = porta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }
}
