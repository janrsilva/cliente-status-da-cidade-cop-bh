/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Janderson
 */
public interface LetreiroInterface extends Remote {

    /**
     * Definição de método obrigatório responsável por verificar se a
     * comunicação com o servidor está on-line.
     *
     * @param mensagem
     * @return String
     * @throws RemoteException
     */
    public String hello_servidor(String mensagem) throws RemoteException;
    
    public boolean show_message(String mensagem) throws RemoteException;
}
