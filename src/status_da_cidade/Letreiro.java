/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.io.File;
import java.io.IOException;
import org.ini4j.Ini;

/**
 *
 * @author Janderson
 */
public class Letreiro extends IniFile {

    private final String message_section    = "message";
    private final String city_color_section = "city-color";
    private final String msg_hr_key         = "msg_hr";
    private final String greeting_key       = "greeting";
    private final String txt_key            = "txt";
    private final String sign_key           = "sign";
    private final String fgcolor_key        = "fgcolor";
    private final String bgcolor_key        = "bgcolor";
    private final String color_key          = "color";
    
    private String msg_hr;
    private String greeting;
    private String txt;
    private String sign;
    private String fgcolor;
    private String bgcolor;
    private String color;

    public Letreiro(String path) {
        super(path);
    }
    public Letreiro(String path, boolean getDados) {
        super(path);
        if(getDados){
            getDadosLetreiroIni();
        }
    }

    public String getMsg_hr() {
        return msg_hr;
    }

    public void setMsg_hr(String msg_hr) {
        this.msg_hr = msg_hr;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getFgcolor() {
        return fgcolor;
    }

    public void setFgcolor(String fgcolor) {
        this.fgcolor = fgcolor;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public boolean getDadosLetreiroIni() {
        File l = new File(getPath());
        try {
            setIni(new Ini(l));
        } catch (IOException ex) {
            return false;
        }
        String[] city_keys = {color_key};
        String[] msg_keys = {msg_hr_key, greeting_key, txt_key, sign_key, fgcolor_key, bgcolor_key};
        if(verificaDados(city_color_section, city_keys) && verificaDados(message_section, msg_keys)){
            msg_hr      = getPropriedade(message_section, msg_hr_key);
            greeting    = getPropriedade(message_section, greeting_key);
            txt         = getPropriedade(message_section, txt_key);
            sign        = getPropriedade(message_section, sign_key);
            fgcolor     = getPropriedade(message_section, fgcolor_key);
            bgcolor     = getPropriedade(message_section, bgcolor_key);
            color       = getPropriedade(city_color_section, color_key);
            return true;
        }
        return false;
    }
    
    public boolean saveDadosLetreiroIni() {
        File l = new File(getPath());
        try {
            setIni(new Ini(l));
        } catch (IOException ex) {
            return false;
        }
        setPropriedade(message_section, msg_hr_key, msg_hr);
        setPropriedade(message_section, greeting_key, greeting);
        setPropriedade(message_section, txt_key, txt);
        setPropriedade(message_section, sign_key, sign);
        setPropriedade(message_section, fgcolor_key, fgcolor);
        setPropriedade(message_section, bgcolor_key, bgcolor);
        setPropriedade(city_color_section, color_key, color);
        return saveIni();
    }
}
