/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import org.ini4j.Ini;

/**
 *
 * @author Janderson
 */
public class Config extends IniFile{
    
    private final String billboardSection   = "cop-bh-billboard"; //Sessão 
    private final String ServerSection      = "cop-bh-servidor-"; //Sessão 
    private final String ip_key             = "ip"; //Sessão 
    private final String port_key           = "porta"; //Sessão 
    private final String city_color         = "city-color"; //Sessão
    private final String color_key          = "color"; //Chave
    private final String note_0             = "note-0"; //Sessão
    private final String txt_key            = "txt"; //Chave
    private final String end_key            = "end"; //Chave
    private final String id_key             = "id"; //Chave
    private ArrayList<InfoServer> servers   = null;
    private String pathLetreiroIni;
    
    public Config(String path) {
        super(path);
    }

    public String getBillboardSection() {
        return billboardSection;
    }

    public String getCity_color() {
        return city_color;
    }

    public String getColor_key() {
        return color_key;
    }

    public String getNote_0() {
        return note_0;
    }

    public String getTxt_key() {
        return txt_key;
    }

    public String getEnd_key() {
        return end_key;
    }

    public String getPathLetreiroIni() {
        return pathLetreiroIni;
    }

    public void setPathLetreiroIni(String pathLetreiroIni) {
        this.pathLetreiroIni = pathLetreiroIni;
    }

    public ArrayList<InfoServer> getServers() {
        return servers;
    }

    public void setServers(ArrayList<InfoServer> servers) {
        this.servers = servers;
    }

    public String getServerSection() {
        return ServerSection;
    }

    public String getIp_key() {
        return ip_key;
    }

    public String getPort_key() {
        return port_key;
    }

    public String getId_key() {
        return id_key;
    }
    
    public static Config criaArquivoConfig(String path, boolean clean) {
        if(clean){
            deleteFile(new File(path), true);
        }
        Config config = new Config(path);
        config.setPathArquivoLetreiro();
        if(config.getPathLetreiroIni() != null){
            config.setServidoresInfo();
            if (config.getServers() != null) {
                if(config.saveDadosConfig()){
                    JPrompt.printPane("Configurações salvas com sucesso!");
                    return config;
                }
            }
        }
        return null;
    }
    
    private String setPathArquivoLetreiro() {
        JPrompt.printPane("Bem vindo!", "É necessário localizar o arquivo Letreiro.ini. \n\nClique em OK para localizar o arquivo.");
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileFilter(new TipoArquivoIni());
        int returnVal = jFileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = jFileChooser.getSelectedFile();
            setPathLetreiroIni(file.getPath());
            return file.getPath();
        }else {
            System.out.println("Acesso ao arquivo cancelado pelo usuário.");
            return null;
        }
    }
    
    private void setServidoresInfo() {
        ArrayList<InfoServer> servidores = new ArrayList<InfoServer>();
        for(int i = 1; i <= 2; i++){
            String ip       = JPrompt.readStringPane("Informações dos Servidores","Digite o IP (ou nome de rede) do servidor "+ i +" :");
            String porta    = JPrompt.readStringPane("Informações dos Servidores","Digite a porta do servidor "+ i +" : \nDê preferência a porta padrão 20161");
            if(ip != null && porta != null){
                servidores.add(new InfoServer(String.valueOf(i), ip, porta));
            } else {
                setServers(null);
                return;
            }
        }
        if(servidores.isEmpty()){
            setServers(null);
        } else {
            setServers(servidores);
        }
    }

    public boolean getDadosConfig() {
        File file = new File(getPath());
        if (file.exists()) {
            try {
                setIni(new Ini(file));
                System.out.println("Ini foi instanciado");
            } catch (IOException ex) {
                if(criaArquivoConfig(getPath(), true) != null){
                    getDadosConfig();
                    return true;
                }
                System.out.println("Erro ao instanciar o arquivo Ini.");
                return false;
            }
            try {
                ArrayList<InfoServer> servidores = new ArrayList<InfoServer>();
                String[] bill_board_keys = {end_key};
                if(verificaDados(billboardSection, bill_board_keys)){
                    String end  = getPropriedade(billboardSection, end_key);
                    setPathLetreiroIni(end);
                } else {
                    if(criaArquivoConfig(getPath(), true) != null){
                        getDadosConfig();
                        return true;
                    }
                    return false;
                }
                for(int i = 1; i <= 2; i++){
                    String id = null, ip = null, porta = null;
                    String[] server_keys = {ip_key, port_key, id_key};
                    if(verificaDados(ServerSection+i, server_keys)){
                        id      = getPropriedade(ServerSection+i, id_key);
                        ip      = getPropriedade(ServerSection+i, ip_key);
                        porta   = getPropriedade(ServerSection+i, port_key);
                        servidores.add(new InfoServer(id, ip, porta));
                    } else {
                        if(criaArquivoConfig(getPath(), true) != null){
                            getDadosConfig();
                            return true;
                        }
                        return false;
                    }
                }
                setServers(servidores.isEmpty() ? null : servidores);
            } catch (NullPointerException ex) {
                Config config = criaArquivoConfig(getPath(), true);
                if(config != null){
                    setPathLetreiroIni(config.getPathLetreiroIni());
                    setServers(config.getServers());
                } else{
                    deleteFile(file, false);
                    return false;
                }
            }
        }
        return getServers() != null && getPathLetreiroIni() != null;
    }
    
    public boolean saveDadosConfig() {
        File f = new File(getPath());
        setIni(new Ini());
        if(getPathLetreiroIni() != null && getServers() != null){
            setPropriedade(getBillboardSection(), getEnd_key(), getPathLetreiroIni());
            for(InfoServer s : getServers()){
                setPropriedade(getServerSection()+s.id, getId_key(), s.id);
                setPropriedade(getServerSection()+s.id, getIp_key(), s.ip);
                setPropriedade(getServerSection()+s.id, getPort_key(), s.porta);
            }
            return saveIni();
        } else {
            System.out.println("Erro no if(getPathLetreiroIni() != null && getServers() != null)");
            return false;
        }
    }
}
