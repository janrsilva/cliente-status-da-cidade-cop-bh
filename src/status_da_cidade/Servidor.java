/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author Janderson
 */
public class Servidor {
    private final String remote_reference = "StatusCidade";
    private final String porta_padrao = "20161";
    private String ip;
    private String porta;
    private String nome;
    private String url;

    public Servidor(String ip, String porta) {
        this.ip = ip;
        this.porta = porta;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUrl() {
        if (this.porta == null || this.porta.isEmpty()) {
            this.porta = this.porta_padrao;
        }
        return "//" + this.ip + ":" + this.porta + "/" + this.remote_reference;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    /**
     * Método responsável por verificar se o servidor está on-line através da
     * tentativa de acesso ao objeto remoto.
     *
     * @return Boolean
     */
    public Boolean helloServidor() {
        try {
            LetreiroInterface letreiro = (LetreiroInterface) Naming.lookup(getUrl());
            letreiro.hello_servidor("Hello Servidor!!!");
//            System.out.println(letreiro.hello_servidor("Hello Servidor!!!") + "\nServidor ON-LINE!");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            System.out.println(getUrl() + "\n Servidor OFF-LINE!" + ex.getMessage());
//            System.out.println("Servidor OFF-LINE ou porta incorreta!" + ex.getMessage());
            return false;
        }
        return true;
    }
    
    public Boolean sendMessage(String mensagem) {
        try {
            LetreiroInterface letreiro = (LetreiroInterface) Naming.lookup(getUrl());
            letreiro.show_message(mensagem);
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            System.out.println(getUrl() + "\n Servidor OFF-LINE!" + ex.getMessage());
            return false;
        }
        return true;
    }
}
