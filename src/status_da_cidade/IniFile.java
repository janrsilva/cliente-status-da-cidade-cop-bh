/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.ini4j.Ini;

/**
 *
 * @author Janderson
 */
public class IniFile{
    Ini ini;
    private String path;

    public IniFile(String path) {
        this.path = path;
    }
    
    public IniFile(File f) throws IOException {
        this.path   = f.getPath();
        System.out.println(path);
        this.ini    = new Ini(f);
    }

    public Ini getIni() {
        return ini;
    }

    public void setIni(Ini ini) {
        this.ini = ini;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public String getPropriedade(String sessao, String chave) {
        Ini.Section section = ini.get(sessao);
        return section.get(chave);
    }
    
    public void setPropriedade(String sessao, String chave, String valor) {
        ini.put(sessao, chave, valor);
    }
    
    public boolean verificaDados(String section, String[] array_keys){
        if(ini != null){
            try {
                System.out.println("\nSessão: "+section+" {");
                Ini.Section s = ini.get(section);
                for(int i=0; i < array_keys.length; ++i){
                    if(s.containsKey(array_keys[i])) {
                        System.out.println("    "+array_keys[i]+": "+s.get(array_keys[i]));
                        s.get(array_keys[i]);
                    }else {
                        return false;
                    }
                }
                System.out.println("}");
                System.out.println("Todas chaves da sessão "+section+" são válidas.");
            } catch(NullPointerException ex){
                return false;
            }
        }else{
            return false;
        }
        return true;
    }
    
    public static void deleteFile(File file, boolean mensagem){
        if(file.exists()){
            if(mensagem){
                JPrompt.printPane("Foram encontrados erros no arquivo \""+ file.getPath() +"\", o arquivo será excluído!");
            }
            file.delete();
        }
    }
    
    public boolean saveIni(){
        String name = "";
        try {
            System.out.println("Tentando salvar... "+ name);
            FileWriter file = new FileWriter(path, false);
            name = file.getClass().getName();
            ini.store(file);
            file.close();
            System.out.println("Log: save() salvou o arquivo "+ name);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Erro ao salvar o arquivo "+ name);
            return false;
        }
    }
    
    public static boolean fileExists(String path){
        File file = new File(path);
        return file.exists();
    }

}
