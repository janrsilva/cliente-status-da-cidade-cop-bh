/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.io.File;

/**
 * Sub classe resposável por definir um tipo de arquivo personalizado, que
 * será localizado para configuração do caminho percorrido até o
 * arquivoletreiro._ini.
 *
 */
public class TipoArquivoIni extends javax.swing.filechooser.FileFilter {

    /**
     * Método obrigatório determina a leitura de apenas diretório ou
     * arquivos de extensão "._ini".
     *
     * @param file
     * @return boolean
     */
    @Override
    public boolean accept(File file) {
        return file.isDirectory() || file.getAbsolutePath().endsWith(".ini");
    }

    /**
     * Método obrigatório determina descrição para o tipo de arquivo
     * "._ini".
     *
     * @return String
     */
    @Override
    public String getDescription() {
        return "Arquivo de Configuração COP-BH (*.ini)";
    }
}
